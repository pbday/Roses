
%Running the solve method below prints the following unique solution. 
%This was verified that it meets all the conditions
%
%	?- solve.
%	ida bought mountainBloom roses and balloons for a wedding.
%	jeremy bought goldenSunset roses and candles for a seniorProm.
%	stella bought cottageBeauty roses and placeCards for a charityAuction.
%	hugh bought pinkParadise roses and streamers for a anniversaryParty.
%	leroy bought sweetDreams roses and chocolates for a retirement.
%	true ;
%	false.

solve:-
	%Set all event, rose type and additional items to different
		event(IEvent), event(JEvent), event(SEvent), event(HEvent), event(LEvent),
		alldifferent([IEvent,JEvent,SEvent,HEvent,LEvent]),
		
		type(IType),type(JType),type(SType),type(HType),type(LType),
		alldifferent([IType,JType,SType,HType,LType]),

		other(IOther),other(JOther),other(SOther),other(HOther),other(LOther),
		alldifferent([IOther,JOther,SOther,HOther,LOther]),

	%Set the solution list
		Solution = [[ida,IEvent,IType,IOther],[jeremy,JEvent,JType,JOther],[stella,SEvent,SType,SOther],
				[hugh,HEvent,HType,HOther],[leroy,LEvent,LType,LOther]],
	
	%1. Jeremy made a purchase for the senior prom. Stella who didn�t choose flowers for a
	% wedding) picked the Cottage Beauty variety.
		JEvent = seniorProm,
		SEvent \= wedding,
		SType = cottageBeauty,

	%2. Hugh (who selected the Pink Paradise blooms) didn�t choose flowers for either thecharity
	%auction or the wedding.	
		HType = pinkParadise,
		HEvent \= charityAuction,
		HEvent \= wedding,
		
	%3. The customer who picked roses for an anniversary party also bought streamers.The
	%one shopping for a wedding chose the balloons.	
		member([_,anniversaryParty,_,streamers],Solution),
		member([_,wedding,_,balloons],Solution),

	%4. The customer who bought the Sweet Dreams variety also bought gourmet chocolates.Jeremy
	%didn�t pick the Mountain Bloom variety.
		member([_,_,sweetDreams,chocolates],Solution),
		JType \= mountainBloom,

	%5. Leroy was shopping for the retirement banquet. The customer in charge of decoratingthe
	%senior prom also bought the candles.
		LEvent = retirement,
		member([_,seniorProm,_,candles],Solution),

	printSolution(Solution).

%5 event types
event(seniorProm).
event(charityAuction).
event(wedding).
event(anniversaryParty).
event(retirement).

%5 types of rose
type(goldenSunset).
type(cottageBeauty).
type(pinkParadise).
type(sweetDreams).
type(mountainBloom).

%5 additional items
other(placeCards).
other(streamers).
other(balloons).
other(chocolates).
other(candles).

%all need to be different - taken "all different" method from example exercise
alldifferent([H|T]) :- member(H,T),!,fail.
alldifferent([_|T]) :- alldifferent(T).
alldifferent([]).

printSolution([H|T]) :- printLine(H), printSolution(T).
printSolution([]).
printLine([Name,Event,Type,Other]) :- write(Name), write(' bought '), write(Type), write(' roses and '), write(Other), write(' for a '), write(Event), write('.'), nl.